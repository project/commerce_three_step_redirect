Commerce Three Step Redirect
This module provides some limited functionality for processing payments in Drupal Commerce by using a payment gateway API known as the "Three Step Redirect" method. This method/API was developed by Network Merchants, LLC but is used by many, many other processors out there, especially ISOs, because NMI resells and repackages the API for various other companies. If your payment gateway uses something called the "Three Step Redirect API", this module will most likely work for you. (Tip: If your gateway's Help/Integration section looks strikingly similar to this manual, then it is compatible with your gateway.)

This module is written based off the patches found in the issue queue of the abandoned Commerce NMI module. The patches do not in any way depend on the base Commerce NMI module, and since it has been abandoned anyway, I pulled them out into their own separate module project which depends only on Commerce and Commerce Payment.

Installation
Install as you would any other module. You will need to adjust the payment method settings in your store configuration. There is a default API key listed which can be used for testing, otherwise you will need to create an API key in your gateway's settings and save it here. If you are using an aforementioned repackaged version of NMI's gateway, you will also need to input the correct API endpoint to post transactions to. Finally, adjust your checkout workflow and ensure that the "Off-site payment redirect" pane is the 1 and only checkout pane listed on the "Payment" checkout page.

Important Limitations
This module is built utilizing Commerce's "Offsite Payment Redirect" functionality. However, please note that this functionality was designed to send the customer to an actual webpage on another domain to submit their payment. This module keeps the end-user on your storefront's domain and also collects their payment information on your own site in the checkout workflow, but modifies the form submit handler to send this information directly to the payment gateway. The customer's sensitive card data never touches Drupal/your server and provides strong compliance with PCI standards (I'm no expert, but I believe this functionality is consistent with SAQ A-EP standards). It works, but this technique presents some challenges as Commerce Payment was never designed to work this way.

In short, this module works to process payments from customers in the end-user payment/checkout workflow. New features may be added, but this module currently does not:

*Provide any Drupal-side payment data validation. This is left entirely up to your gateway to validate the data and decline improperly entered card information. If you choose to use this module in production, you are strongly encouraged to review your card-acceptance policies in your gateway's settings page, especially AVS and CVV rejection rules.
*Provide any Administration interface functionality with the gateway. Payments can only be processed via the customer-facing checkout flow
*Provide any other transaction options than "Sale". There is no option/interface within Drupal or Commerce to process Voids, Refunds, etc. You will need to login directly to the gateway to perform these functions manually. There is no post-back of changes like these from your gateway to your Commerce store
*Provide functionality or interact with the 'Customer Vault' or 'Recurring' options with the gateway.

Help Wanted
I am by no means an expert at this and this project is my very first sandbox project. Ideas, tips, pointers, recommendations, patches, or offers of co-maintainership are welcome! Also, I have no plans to promote this to "full project" status unless someone more experienced than me offers to become a co-maintainer to accomplish this.
