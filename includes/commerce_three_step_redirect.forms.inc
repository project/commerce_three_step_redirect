<?php

/**
 * @file
 * API, hook, and form implementations of Commerce Payment module.
 * See Commerce Payment module API.
 */

 /**
  * Default settings.
  */
function commerce_three_step_redirect_default_settings() {
  return array(
    'api_key' => '',
    'post_url' => '',
    'debug' => FALSE,
  );
}

/**
 * Payment method callback: settings form.
 */
function commerce_three_step_redirect_settings_form($settings = NULL) {

  // Add a form element to record the gateway API key.
  $form['api_key'] = array(
    '#type' => 'textfield',
    '#title' => t('Merchant API Key'),
    '#default_value' => $settings['api_key'],
    '#required' => TRUE,
    '#description' => t('API key for the merchant account that transactions will be processed for. Use "2F822Rw39fx762MaV7Yy86jXGTC7sCDy" for testing.'),
  );

  $form['post_url'] = array(
    '#type' => 'textfield',
    '#title' => t('Payment gateway URL'),
    '#default_value' => $settings['post_url'],
    '#required' => TRUE,
    '#description' => t('Payment gateway URL for processing transactions. See your gateway\'s documentation for this. It will follow the pattern: "https://secure.YOURGATEWAY.com/api/v2/three-step".'),
  );

  $form['debug'] = array(
    '#type' => 'checkbox',
    '#title' => t('Log Requests and Responses'),
    '#description' => t('Step 1 and 3 requests/responses can be logged for debugging purposes. Recommend that this be turned OFF in production. To prevent Drupal from handling the customer\'s payment card info, Step 2 cannot ever be logged.'),
    '#default_value' => $settings['debug'],
  );

  return $form;
}

/**
 * Implementation of CALLBACK_commerce_payment_method_redirect_form.
 *
 * This is a bit clunky, because the redirect function was designed to take
 * the user completely away from the site and expect them to return. By
 * contrast, we are not actually leaving the site, we are simply redirecting
 * the user's card data to the gateway from a form on our own site.
 *
 * Step 1 submits order details and generates a redirect url.
 * This is done in the background by Drupal when the user proceeds to the
 * payment page. The payment gateway returns a "form-url", which is the
 * URL the customer should submit their sensitive payment data to directly.
 * We must set this URL to be the action of the form "submit" button and
 * present the form to the User.
 *
 * Step 2 is the process of the client entering their payment data and sending
 * it directly to the gateway. Since the data is sent directly to the gateway,
 * it is never touched by Drupal. Therefore, all payment data validation is
 * performed by the gateway; there is NO Drupal-side data validation.
 *
 * The payment gateway collects the payment data, matches it to the record
 * we sent in Step 1, and returns a token to Drupal.
 *
 * @return string
 *   The secure url to post the payment form details to.
 */
function commerce_three_step_redirect_redirect_form($form, &$form_state, $order, $payment_method) {
  module_load_include('steps.inc', 'commerce_three_step_redirect', 'includes/commerce_three_step_redirect');

  // This is the destination url which the payment gateway should return to.
  // Commerce will continue with step 3 and final checkout steps upon return.
  $destination = url('checkout/' . $order->order_id . '/payment/return/' . $order->data['payment_redirect_key'], array('absolute' => TRUE));
  // Generate an authenticated payment url (step 1) based on the order details.
  $authentication_url = commerce_three_step_redirect_step_1($order, $payment_method, $destination);

  if (empty($authentication_url) || !is_string($authentication_url)) {
    // No payment url was returned, which means the module is not configured
    // properly or something has gone wrong with the remote connection.
    drupal_set_message(t('Communication with the payment gateway failed. Transaction not processed. Please try again later. Contact the website administrator if the problem persists.'), 'error');
    drupal_goto('cart');
  }

  // Generate our secure form and present it to the user (step 2).
  return commerce_three_step_redirect_step_2($form, $authentication_url);
}

/**
 * Implementation of CALLBACK_commerce_payment_method_redirect_form_validate.
 *
 * Once the customer has completed Step 2, a token variable is returned from the gateway.
 * Drupal automatically proceeds to the validation step.
 *
 * Step 3 validates the customer's payment by simply resubmitting this token to the gateway along
 * with the merchant's API key. The gateway returns transaction details to Drupal and the process
 * is complete. Step 3's primary purpose is to prevent man-in-the-middle attacks.
 *
 * @return bool
 *   FALSE if an error occured and the user should be sent back a step in the order process.
 */
function commerce_three_step_redirect_redirect_form_validate($order, $payment_method) {
  module_load_include('steps.inc', 'commerce_three_step_redirect', 'includes/commerce_three_step_redirect');
  module_load_include('credit_card.inc', 'commerce_payment', 'includes/commerce_payment');

  // The payment gateway redirects back to the checkout process and adds a
  // security token which we will validate as part of step 3.
  $token = trim($_GET['token-id']);
  if (empty($token)) {
    // There was no token present, so something has gone wrong.
    drupal_set_message(t('Communication with the payment gateway failed. Transaction not processed. Please try again later. Contact the website administrator if the problem persists.'), 'error');
    return FALSE;
  }

  // Validate the security token and create a commerce transaction for the order.
  $success = commerce_three_step_redirect_step_3($order, $payment_method, $token);
  if (!$success) {
    // An error occurred during step 3.
    return FALSE;
  }
}
