(function($) {

/**
 * Disable the continue buttons in the checkout process once they are clicked
 * and provide a notification to the user.
 */
Drupal.behaviors.threeStepPayment = {
  attach: function (context, settings) {
    // When the buttons to move from page to page in the checkout process are
    // clicked we disable them so they are not accidently clicked twice.
    $('form[name="commerce_three_step_redirect_step_2"]:not(.checkout-processed)', context).addClass('checkout-processed').submit(function() {
      var $this = $(this);
      var $submit = $('input.checkout-authorise', $this);
      $submit.clone().insertAfter($submit).attr('disabled', true).next().removeClass('element-invisible');
      $submit.hide();
    });
  }
}

})(jQuery);
