<?php

/**
 * @file
 * Functions for the Commerce Three Step Redirect module, to perform the 3 key steps.
 */

// Define payment gateway response codes.
define('COMMERCE_THREE_STEP_REDIRECT_REQUEST_APPROVED', 1);
define('COMMERCE_THREE_STEP_REDIRECT_REQUEST_DECLINED', 2);
define('COMMERCE_THREE_STEP_REDIRECT_REQUEST_ERROR', 3);

/**
 * Exception class to deal with payment gateway errors.
 */
class ThreeStepException extends Exception {
  protected $response;

  /**
   *
   */
  public function __construct($response = NULL, $message = NULL, $code = 0, Exception $previous = NULL) {
    $this->response = $response;
    if (!isset($message) && isset($this->response)) {
      // Create an error message from the response data.
      $error_message = array(
        '<strong>' . $response->{'result-text'} . '</strong>',
        t('Response: @resultcode', array('@resultcode' => commerce_three_step_redirect_result_code($response->{'result-code'}))),
        t('Transaction Type: ' . ucwords($response->{'action-type'})),
        t('AVS Response: @avs', array('@avs' => commerce_three_step_redirect_avs($response->{'avs-result'}))),
        t('CVV Response: @cvv', array('@cvv' => commerce_three_step_redirect_cvv($response->{'cvv-result'}))),
      );
      $message = implode('<br />', $error_message);
    }
    parent::__construct($message, $code, $previous);
  }

  /**
   *
   */
  public function __toString() {
    // Load our error message.
    $error = (string) $this->getMessage();
    // Change gateway field names in the error message to Drupal field names.
    $error = str_replace(array(' ccnumber ', ' ccexp ', ' ... '), array(' Card number ', ' Security code ', ' Expiration date '), $error);
    // Remove the gateway reference code from the message.
    $error = preg_replace('/REFID:.*$/', '', $error);
    // Ensure there is a fullstop at the end of the message for consistency, as
    // it is sometimes omitted by the payment gateway.
    return t(rtrim(trim($error), '.') . '.');
  }

  /**
   *
   */
  public function getResponse() {
    return $this->response;
  }

}

/**
 * Perform a payment gateway request and process the response.
 *
 * @param $xml
 *   A DOMDocument object or xml formatted string to send to the payment gateway.
 * @param $url
 *   The url to send the request to.
 */
function commerce_three_step_redirect_request($xml, $url) {
  // Create our request.
  $options = array(
    'headers' => array('Content-type' => 'text/xml'),
    'method' => 'POST',
    'data' => is_string($xml) ? $xml : $xml->saveXML(),
  );

  // Send the request to the remote url.
  $response = drupal_http_request($url, $options);
  if (!empty($response->error) || empty($response->data)) {
    // Something went wrong with the request. This could mean the gateway is
    // unavailable or the module isn't correctly configured.
    throw new Exception($response->error);
  }

  // Parse the XML data from the response.
  $response = @new SimpleXMLElement($response->data);
  switch ((string) $response->result) {
    case COMMERCE_THREE_STEP_REDIRECT_REQUEST_APPROVED:
      // The request was successful.
      return $response;

    case COMMERCE_THREE_STEP_REDIRECT_REQUEST_DECLINED:
      // The request completed, but was denied by the gateway.
      throw new ThreeStepException($response);

    default:
    case COMMERCE_THREE_STEP_REDIRECT_REQUEST_ERROR:
      // An error occurred during the request.
      throw new ThreeStepException($response);
  }
}

/**
 * Perform step 1 of the authorization process. Here we submit the order data
 * to the payment gateway. If successful, the gateway should respond with a URL
 * which we will use as the submit button's endpoint in Step 2.
 *
 * @param $order
 *   The commerce order object.
 * @param $payment_method
 *   The commerce payment_method object.
 * @param $destination
 *   The destination url for the gateway to redirect back to.
 */
function commerce_three_step_redirect_step_1($order, $payment_method, $destination) {
  // Create an XML document, so we can build our request.
  $xml = new DOMDocument('1.0', 'UTF-8');
  $xml->formatOutput = TRUE;

  if (get_class($order) != 'EntityDrupalWrapper') {
    $order = entity_metadata_wrapper('commerce_order', $order);
  }
  // Load the order amount.
  $total = $order->commerce_order_total->value();
  $currency = $total['currency_code'];

  // Add the sale information.
  $xmlOrder = $xml->createElement('sale');
  $xmlOrder->appendChild(new DOMElement('api-key', $payment_method['settings']['api_key']));
  $xmlOrder->appendChild(new DOMElement('redirect-url', $destination));
  $xmlOrder->appendChild(new DOMElement('amount', commerce_currency_amount_to_decimal($total['amount'], $currency)));
  $xmlOrder->appendChild(new DOMElement('ip-address', $order->hostname->value()));
  $xmlOrder->appendChild(new DOMElement('currency', $currency));
  $xmlOrder->appendChild(new DOMElement('order-id', $order->order_id->value()));

  // Determine the order level item amount and tax amount line items. To prevent
  // rounding problems getting in the way, we calculate them based on the order
  // total instead of tallying it from each line item.
  if (module_exists('commerce_tax')) {
    $tax = commerce_round(COMMERCE_ROUND_HALF_UP, commerce_tax_total_amount($total['data']['components'], FALSE, $currency));
  }
  else {
    $tax = 0;
  }
  $xmlOrder->appendChild(new DOMElement('tax-amount', commerce_currency_amount_to_decimal($tax, $currency)));
  foreach (array('billing' => 'billing', 'shipping' => 'shipping') as $element_type => $address_type) {
    $address = (object) $order->{"commerce_customer_{$address_type}"}->commerce_customer_address->value();
    $xmlAddress = $xml->createElement($element_type);

    // Set the Billing and Shipping addresses from the user entered details.
    @list($first_name, $last_name) = explode(' ', trim($address->name_line), 2);

    $xmlAddress->appendChild(new DOMElement('first-name', $address->first_name ? $address->first_name : $first_name));
    $xmlAddress->appendChild(new DOMElement('last-name', $address->last_name ? $address->last_name : $last_name));
    $xmlAddress->appendChild(new DOMElement('address1', $address->thoroughfare));
    // ToDo: Find out what this is called and fix it.
    $xmlAddress->appendChild(new DOMElement('address2', ''));
    $xmlAddress->appendChild(new DOMElement('city', $address->locality));
    $xmlAddress->appendChild(new DOMElement('state', $address->administrative_area));
    $xmlAddress->appendChild(new DOMElement('postal', $address->postal_code));
    $xmlAddress->appendChild(new DOMElement('country', $address->country));
    $xmlAddress->appendChild(new DOMElement('company', $address->organisation_name));
    // ToDo: Commerce does not collect phone numbers by default. Many merchants do, but this field can
    // be generated with any of a variety of names. To add this field to the request, we need to
    // provide options in the UI to define this field and use it below. This is particularly important
    // for international billing and shipment, which often REQUIRE phone numbers to charge cards or
    // generate shipping labels.
    $xmlAddress->appendChild(new DOMElement('phone', ''));

    if ($element_type == 'billing') {
      $xmlAddress->appendChild(new DOMElement('email', $order->mail->value()));
    }

    $xmlOrder->appendChild($xmlAddress);
  }

  $shipping_total = 0.0;
  foreach ($order->commerce_line_items as $delta => $line_item) {
    $type = $line_item->type->value();
    if ($type == 'product') {
      // Products already chosen by user.
      $xmlProduct = $xml->createElement('product');
      $sku = $line_item->line_item_label->value();
      $xmlProduct->appendChild(new DOMElement('product-code', $sku));
      if ($product = commerce_product_load_by_sku($sku)) {
        $xmlProduct->appendChild(new DOMElement('description', $product->title));
      }
      $xmlProduct->appendChild(new DOMElement('quantity', $line_item->quantity->value()));
      $unit_cost = $line_item->commerce_unit_price->value();
      $xmlProduct->appendChild(new DOMElement('unit-cost', commerce_currency_amount_to_decimal($unit_cost['amount'], $currency)));
      $total_cost = $line_item->commerce_total->value();
      $xmlProduct->appendChild(new DOMElement('total-amount', commerce_currency_amount_to_decimal($total_cost['amount'], $currency)));
      $xmlOrder->appendChild($xmlProduct);
    }
    elseif ($type == 'shipping') {
      $total_cost = $line_item->commerce_total->value();
      $shipping_total += commerce_currency_amount_to_decimal($total_cost['amount'], $currency);
    }
  }
  $xmlOrder->appendChild(new DOMElement('shipping-amount', $shipping_total));

  $xml->appendChild($xmlOrder);
  // Log to watchdog if debugging enabled.
  if ($payment_method['settings']['debug'] == TRUE) {
    $log_xml = $xml->saveXML();
    $post_url = $payment_method['settings']['post_url'];
    watchdog('commerce_three_step_redirect', 'Three Step Redirect (Step 1) request to @post_url: !param', array('@post_url' => $post_url, '!param' => '<pre>' . check_plain($log_xml) . '</pre>'), WATCHDOG_DEBUG);
  }
  // Process Step One: Submit all transaction details to the Payment Gateway except the customer's sensitive payment information.
  // The Payment Gateway will return a variable form-url.
  try {
    $response = commerce_three_step_redirect_request($xml, $payment_method['settings']['post_url']);
    // Log to watchdog if debugging enabled.
    if ($payment_method['settings']['debug'] == TRUE) {
      $log_response = $response->saveXML();
      watchdog('commerce_three_step_redirect', 'Three Step Redirect (Step 1) response from @post_url: !param', array('@post_url' => $post_url, '!param' => '<pre>' . check_plain($log_response) . '</pre>'), WATCHDOG_DEBUG);
    }
    return (string) $response->{'form-url'};
  }
  // If we make it to here, something went wrong. Log the error messages.
  catch (ThreeStepException $e) {
    watchdog('commerce_three_step_redirect', 'Gateway error (step 1): !error', array('!error' => $e->getMessage()), WATCHDOG_ERROR);
    return;
  }
  catch (Exception $e) {
    watchdog('commerce_three_step_redirect', 'Gateway error (step 1): !error', array('!error' => (string) $e), WATCHDOG_ERROR);
    return;
  }
}

/**
 * This generates the user form required to perform step 2.
 * We cannot use the pre-built form in the Commerce Payment module, because
 * we need the machine names of the fields to match the XML expected by
 * by the payment gateway.
 *
 * This prevents us from using any prebuilt Commerce Payment validation, but
 * enhances security by ensuring the customer's sensitive payment data never
 * actually touches the site.
 *
 * @param unknown $form
 * @param unknown $redirect_url
 *
 * @return multitype:string multitype:multitype:string   NULL
 */
function commerce_three_step_redirect_step_2($form, $authentication_url) {
  // Sets the action of the form to submit to the authentication URL generated by step 1.
  $form['#action'] = $authentication_url;
  $form['#attributes'] = array('name' => 'commerce_three_step_redirect_step_2');

  // Prepare the fields to include on the credit card form.
  // Add the sensitive payment fields to the form. These will be submitted
  // securely straight to the payment gateway, bypassing Drupal.
  // Using this form as it stands produces PCI SAQ A-EP level of compliance.
  // Although the user entry could be manipulated or validated through this form, doing
  // so would result in a SAQ level of D, defeating the whole purpose of this process.
  // The point is that we are submitting the user's entry directly to the processor without
  // alteration.
  $form['billing-cc-number'] = array(
    '#type' => 'textfield',
    '#title' => t('Card number'),
    '#description' => t('Your full card number, no spaces, no dashes.'),
    '#maxlength' => 19,
    '#size' => 20,
    '#required' => TRUE,
    '#attributes' => array(
      'required' => 'required',
      'pattern' => '[0-9]{12,19}',
      'title' => t('Enter your full credit/debit card number'),
    ),
  );
  $form['billing-cc-exp'] = array(
    '#type' => 'textfield',
    '#title' => t('Expiration Date (MMYY)'),
    '#description' => t('Format MUST be MMYY - 4 digits, no space, no slash.'),
    '#maxlength' => 4,
    '#size' => 4,
    '#required' => TRUE,
    '#attributes' => array(
      'required' => 'required',
      'pattern' => "(0[1-9]|1[0-2])([1-9][0-9])",
      'title' => t('Enter your expiration date (MMYY)'),
    ),
  );
  $form['cvv'] = array(
    '#type' => 'textfield',
    '#title' => t('Security Code'),
    '#description' => t('Enter the 3 digit code on the back of your card, or the 4 digit code on the front for American Express cards.'),
    '#maxlength' => 4,
    '#size' => 4,
    '#required' => TRUE,
    '#attributes' => array(
      'required' => 'required',
      'pattern' => '[0-9]{3,4}',
      'title' => t('Enter the security code from the back of your credit/debit card'),
    ),
  );

  // Add the confirmation button. This will submit the form directly to the
  // authenticated payment gateway URL (created in step 1).
  $form['buttons'] = array(
    '#type' => 'fieldset',
    '#attributes' => array('class' => array('checkout-buttons')),
  );
  $form['buttons']['continue'] = array(
    '#type' => 'submit',
    '#attributes' => array('class' => array('checkout-authorise')),
    '#value' => t('Submit Payment'),
    '#attached' => array('js' => array(drupal_get_path('module', 'commerce_three_step_redirect') . '/includes/js/three_step_payment.js')),
    '#suffix' => '<span class="checkout-processing element-invisible"></span>',
  );

  return $form;

}

/**
 * Perform Step 3. If the customer's payment was successful, they are returned
 * a token. All we have to do is accept the token and submit it once more to
 * the payment gateway. If the client's token matches ours, the gateway processes
 * the payment.
 *
 * The gateway will always return a response, whether the right or wrong
 * token is submitted. If we receive no response at all,  there are
 * communication problems with the gateway.
 *
 * If the payment was successful, we create a Commerce Transaction object to
 * append to the order. If not successful, determine the type of problem and
 * set appropriate error messages and logs.
 */
function commerce_three_step_redirect_step_3($order, $payment_method, $token) {
  $xml = new DOMDocument('1.0', 'UTF-8');
  $xml->formatOutput = TRUE;

  $xmlAction = $xml->createElement('complete-action');
  $xmlAction->appendChild(new DOMElement('api-key', $payment_method['settings']['api_key']));
  $xmlAction->appendChild(new DOMElement('token-id', $token));

  $xml->appendChild($xmlAction);

  // Prepare a transaction object to log the successful API response.
  $transaction = commerce_payment_transaction_new($payment_method['method_id'], $order->order_id);
  // Add payment method information to the transaction.
  $transaction->instance_id = $payment_method['instance_id'];
  $transaction->payment_method = $payment_method['method_id'];
  $transaction->instance_id = $payment_method['instance_id'] ? $payment_method['instance_id'] : ($payment_method['method_id'] . '|commerce_payment_' . $payment_method['method_id']);
  // Log to watchdog if debugging enabled.
  if ($payment_method['settings']['debug'] == TRUE) {
    $log_xml = $xml->saveXML();
    $post_url = $payment_method['settings']['post_url'];
    watchdog('commerce_three_step_redirect', 'Three Step Redirect (Step 3) request to @post_url: !param', array('@post_url' => $post_url, '!param' => '<pre>' . check_plain($log_xml) . '</pre>'), WATCHDOG_DEBUG);
  }
  try {
    // Process Step Three.
    $response = commerce_three_step_redirect_request($xml, $payment_method['settings']['post_url']);
    // Log to watchdog if debugging enabled.
    if ($payment_method['settings']['debug'] == TRUE) {
      $log_response = $response->saveXML();
      watchdog('commerce_three_step_redirect', 'Three Step Redirect (Step 3) response from @post_url: !param', array('@post_url' => $post_url, '!param' => '<pre>' . check_plain($log_response) . '</pre>'), WATCHDOG_DEBUG);
    }
    // Verify that the response is valid for this order.
    if ($response->result == COMMERCE_THREE_STEP_REDIRECT_REQUEST_APPROVED
        && (string) $response->{'result-text'} === 'SUCCESS') {
      // Set the transaction status.
      $transaction->status = COMMERCE_PAYMENT_STATUS_SUCCESS;
    }
    else {
      $transaction->status = COMMERCE_PAYMENT_STATUS_PENDING;
    }
    // Add the order details to this transaction.
    $transaction->amount = commerce_currency_decimal_to_amount((string) $response->amount, (string) $response->currency);
    $transaction->currency_code = (string) $response->currency;
    $transaction->payload[REQUEST_TIME] = $response->asXML();
    $transaction->remote_id = (string) $response->{'transaction-id'};
    $transaction->remote_status = (string) $response->{'action-type'};

    // Build a meaningful message for the transaction response.
    $message = array(
      '<strong>' . (string) $response->{'result-text'} . '</strong>',
      t('Response: @resultcode', array('@resultcode' => commerce_three_step_redirect_result_code($response->{'result-code'}))),
      t('Transaction Type: ' . ucwords((string) $response->{'action-type'})),
      t('AVS Response: @avs', array('@avs' => commerce_three_step_redirect_avs($response->{'avs-result'}))),
      t('CVV Response: @cvv', array('@cvv' => commerce_three_step_redirect_cvv($response->{'cvv-result'}))),
    );
    $transaction->message = implode('<br />', $message);
  }
  catch (ThreeStepException $e) {
    // The transaction was processed but was declined or rejected by the payment processor. Set an error message.
    $transaction->status = COMMERCE_PAYMENT_STATUS_FAILURE;
    // Log the original error message, not the user-friendly message.
    $transaction->message = (string) $e->getMessage();
    watchdog('commerce_three_step_redirect', 'Gateway error (step 3): !error', array('!error' => (string) $e), WATCHDOG_ERROR);
    // Set an error message for the user. They will be redirected back to the
    // order details page where they can amend their details and retry.
    drupal_set_message(t('The transaction was processed successfully, but was declined or rejected by the payment gateway. The gateway\'s message is: <br />' . (string) $e, 'error'));
  }
  catch (Exception $e) {
    $message = (string) $e;
    // The transaction was NOT processed due to communication problems or faulty data. Set an error message.
    $transaction->status = COMMERCE_PAYMENT_STATUS_FAILURE;
    $transaction->message = $message;
    // Log the exception as it could be a network or configuration issue.
    watchdog('commerce_three_step_redirect', 'Gateway error (step 3): !error', array('!error' => $message), WATCHDOG_ERROR);
    drupal_set_message(t('Something went wrong while communicating with the gateway. Your transaction was NOT processed. Please try again later. Contact the website administrator if the problem persists.'), 'error');
  }

  // Save the transaction information.
  commerce_payment_transaction_save($transaction);

  // TRUE if the payment was successful.
  return (
      $transaction->status === COMMERCE_PAYMENT_STATUS_SUCCESS ||
    $transaction->status === COMMERCE_PAYMENT_STATUS_PENDING
  );
}
